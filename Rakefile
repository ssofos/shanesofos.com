# frozen_string_literal: true

require 'date'
require 'rspec/core/rake_task'
require 'rubocop/rake_task'

RSpec::Core::RakeTask.new
RuboCop::RakeTask.new

def version
  return DateTime.now.iso8601 + '-' + %x(git log -n1 --format=%h).chomp
end

def version_mkdocs
  return File.read('mkdocs.yml').scan(/\d*-\d*-\d*T{1}\d*:\d*:\d*-\d*:\d*-\w*/).first
end

task default: %i[spec rubocop]

desc 'build mkdocs static site'
task :build_mkdocs do
  system 'mkdocs build --clean'
end

desc 'build custom Docker Image in this repo'
task :docker_build do
  system "docker build --build-arg 'site_version=#{version_mkdocs}' -t 'shanesofos.com:local' ."
end

desc 'run Cargo tests'
task :cargo_test do
  system 'cargo test --manifest-path whoami/Cargo.toml'
end

desc 'run custom Docker Image locally'
task :docker_run do
  system 'docker run --rm --name shanesofos.com -p 127.0.0.1:8080:8080 shanesofos.com:local'
end

desc 'install Python requirements with Pip'
task :install_mkdocs do
  system 'pip install --user -r requirements.txt'
  system 'pip install --user -r requirements-test.txt'
end

desc 'install PyPi pur package'
task :install_pur do
  system 'pip install --user pur'
end

desc 'install Cloudflare wrangler'
task :install_wrangler do
  system 'npm install wrangler'
  system './node_modules/.bin/wrangler --version'
end

desc 'run Rubocop linter'
task :lint do
  Rake::Task[:rubocop].invoke
end

desc 'open local mkdocs'
task :open_mkdocs do
  system 'xdg-open http://127.0.0.1:8000'
end

desc 'run Pytests'
task :pytest do
  system 'pytest -v'
end

desc 'publish and deploy shanesofos.com/info/net using wrangler'
task :publish do
  system 'npx wrangler -c wrangler.toml deploy'
end

desc 'generate resume Markdown and PDF files'
task :resume do
  system './resume_generate.rb'
end

desc 'run RSpec tests'
task :test do
  puts 'Running specs...'
  Rake::Task[:spec].invoke
end

desc 'start mkdocs internal local web server for development and testing'
task :test_mkdocs do
  system 'mkdocs serve'
end

desc 'update Python PyPi packages'
task :update_py do
  system 'pur -r requirements.txt'
  system 'pur -r requirements-test.txt'
end

desc 'update the site version in mkdocs.yml'
task :version_update do
  puts 'Updating new mkdocs.yml version'
  new_mkdocs = File.read('mkdocs.yml').gsub(/\d*-\d*-\d*T{1}\d*:\d*:\d*-\d*:\d*-\w*/, version)
  File.open('mkdocs.yml', 'r+') do |f|
    f.write(new_mkdocs.chomp)
  end
end
