from python:3.9-alpine AS sitebuild

# Install GCC on Alpine Linx for PyPi package builds
RUN apk add build-base git

# Install the mkdocs Python dependencies
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

# Copy over the mkdocs site configuration and MarkDown documents
COPY mkdocs.yml mkdocs.yml
COPY mkdocs mkdocs
COPY .git .git

# Build the mkdocs site
RUN mkdocs build --config-file mkdocs.yml --clean --site-dir /site

from nginx:1.21.6-alpine

# Ensure Nginx static site directory exists
RUN mkdir -p /usr/share/nginx/shanesofos.com/
COPY --from=sitebuild /site /usr/share/nginx/shanesofos.com/site

# Configure Nginx with custom configs
COPY nginx/nginx.conf /etc/nginx/nginx.conf
COPY nginx/default.conf /etc/nginx/conf.d/default.conf

EXPOSE 80/tcp 443/tcp 8080/tcp 8443/tcp
