'''Pytest the Identity Class'''

from identity import Identity

IDENTITY = Identity('Tom Bombadil', ['Merry Fellow', 'Master of wood, water and hill'])

def test_identity_name():
  '''Test the Identity Class name attribute'''
  assert isinstance(IDENTITY.name, str)

def test_identity_professions():
  '''Test the Identity Class professions attribute'''
  assert isinstance(IDENTITY.professions, list)

def test_identity_whoami(capsys):
  '''Test the Identity Class whoami method'''
  IDENTITY.whoami()
  identity_captured = capsys.readouterr()
  assert identity_captured.out == 'My name is Tom Bombadil. ' \
          + 'I am an enthusiastic Merry Fellow, Master of wood, water and hill.\n'
