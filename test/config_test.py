'''Test Repository Configuration Files'''
import os

import pytest

CONFIG_FILES=[
  ('Dockerfile'),
  ('Gemfile'),
  ('Gemfile.lock'),
  ('pytest.ini'),
  ('Rakefile'),
  ('requirements.txt'),
  ('resume_generate.rb'),
  ('resume_ssofos.erb'),
  ('whoami.py'),
  ('whoami.rb'),
  ('wrangler.toml'),
]

@pytest.mark.parametrize('config_file', CONFIG_FILES)
def test_config_files(config_file):
  '''Test if a config file exists in the repo'''
  assert os.path.exists(config_file)
