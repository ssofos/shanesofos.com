'''Test Repository Mkocs Files'''
import os
import yaml

import pytest
import material.extensions.emoji

MKDOCS_CONFIG=yaml.load(open('mkdocs.yml'), Loader=yaml.FullLoader)
MKDOCS_FILES=[
  ('mkdocs.yml'),
  ('mkdocs'),
  ('mkdocs/index.md'),
  ('mkdocs/img'),
  ('mkdocs/projects'),
  ('mkdocs/resume'),
]
MKDOCS_CONFIG_KEYS=[
  ('site_name'),
  ('site_author'),
  ('site_description'),
  ('site_url'),
  ('copyright'),
  ('docs_dir'),
  ('edit_uri'),
  ('extra'),
  ('markdown_extensions'),
  ('nav'),
  ('plugins'),
  ('repo_name'),
  ('repo_url'),
  ('theme'),
]
MKDOCS_CONFIG_VALUES=[
  ('site_name', 'Shane Sofos'),
  ('site_author', 'ssofos[at]gmail[dot]com'),
  ('site_description', "Shane Sofos's Site"),
  ('site_url',  'https://shanesofos.com'),
  ('docs_dir', 'mkdocs'),
  ('edit_uri', 'mkdocs'),
  ('markdown_extensions', ['admonition', 'attr_list', 'codehilite', 'meta', 'pymdownx.superfences', {'pymdownx.tabbed': {'alternate_style': True}}, {'pymdownx.emoji': {'emoji_index': material.extensions.emoji.twemoji, 'emoji_generator': material.extensions.emoji.to_svg}}]),
  ('plugins', ['git-revision-date', 'search', {'minify': {'minify_html': True, 'minify_js': True} }]),
  ('repo_name', 'ssofos/shanesofos.com'),
  ('repo_url', 'https://gitlab.com/ssofos/shanesofos.com/blob/master'),
]

def test_mkdocs_config():
  '''Test if mkdocs YAML config has loaded as a Dictionary'''
  assert isinstance(MKDOCS_CONFIG, dict)

@pytest.mark.parametrize('config_file', MKDOCS_FILES)
def test_mkdocs_files(config_file):
  '''Test if certain mkdocs files exists in this repo'''
  assert os.path.exists(config_file)

@pytest.mark.parametrize('key', MKDOCS_CONFIG_KEYS)
def test_mkdocs_config_keys(key):
  '''Test existence of various dict keys in mkdocs YAML config'''
  assert key in MKDOCS_CONFIG

@pytest.mark.parametrize('key,value', MKDOCS_CONFIG_VALUES)
def test_mkdocs_config_values(key,value):
  '''Test correct values of main keys in mkdocs YAML config'''
  assert MKDOCS_CONFIG.get(key) == value

def test_mkdocs_config_theme():
  '''Test correct values of the mkdocs YAMK config theme section'''
  assert MKDOCS_CONFIG['theme']['icon'].get('logo') == 'material/pac-man'
  assert MKDOCS_CONFIG['theme'].get('name') == 'material'
  assert MKDOCS_CONFIG['theme'].get('favicon') == 'img/favicon.ico'
  assert MKDOCS_CONFIG['theme']['palette'][0].get('scheme') == 'slate'
  assert MKDOCS_CONFIG['theme']['palette'][0].get('primary') == 'black'
  assert MKDOCS_CONFIG['theme']['palette'][0].get('accent') == 'deep orange'
  assert MKDOCS_CONFIG['theme']['palette'][0]['toggle'].get('icon') == 'material/toggle-switch'
  assert MKDOCS_CONFIG['theme']['palette'][1].get('scheme') == 'default'
  assert MKDOCS_CONFIG['theme']['palette'][1].get('primary') == 'black'
  assert MKDOCS_CONFIG['theme']['palette'][1].get('accent') == 'deep orange'
  assert MKDOCS_CONFIG['theme']['palette'][1]['toggle'].get('icon') == 'material/toggle-switch-off-outline'
  assert MKDOCS_CONFIG['theme']['features'][0] == 'navigation.expand'
  assert MKDOCS_CONFIG['theme']['features'][1] == 'navigation.tabs'
  assert MKDOCS_CONFIG['theme']['features'][2] == 'navigation.instant'
