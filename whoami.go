package main

import (
	"fmt"

	"gitlab.com/ssofos/shanesofos.com/internal/Static"
)

type identity struct {
	Name        string
	Professions string
}

func main() {
	for _, id := range Static.MyIdentity {
		fmt.Printf("My name is %v. I am an enthusiastic %v.", id.Name, id.Professions)
	}
}
