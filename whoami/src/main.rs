use crate::whoami::Person;

mod whoami;

fn main() {
    let me: whoami::Whoami = whoami::Person::new(
        "Shane R. Sofos",
        vec![
            "Container Infrastructure Engineer",
            "Site Reliability Engineer",
            "Systems Engineer",
            "Penguin Wrangler",
            "Pythonist",
            "Rubyist",
            "Rustacean",
            "GNU Herder",
            "Midgardian",
            "Westerosi",
            "and Child of Ilúvatar",
        ],
    );
    println!("{}", whoami::about(me.name, me.professions()));
}
