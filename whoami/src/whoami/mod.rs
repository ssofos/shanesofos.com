pub struct Whoami {
    pub name: &'static str,
    pub professions: Vec<&'static str>,
}

pub trait Person {
    fn new(name: &'static str, professions: Vec<&'static str>) -> Self;
    fn name(&self) -> &'static str;
    fn professions(&self) -> String;
}

impl Person for Whoami {
    fn new(name: &'static str, professions: Vec<&'static str>) -> Whoami {
        Whoami { name, professions }
    }

    fn name(&self) -> &'static str {
        self.name
    }

    fn professions(&self) -> String {
        self.professions.join(", ")
    }
}

pub fn about(name: &str, professions: String) -> String {
    format!("My name is {}. I am an enthusiastic {}.", name, professions)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_whoami_new() {
        let person: Whoami = Person::new("foobar", vec!["fizz", "buzz"]);
        assert_eq!(person.name, "foobar");
        assert_eq!(person.professions, vec!["fizz", "buzz"]);
    }

    #[test]
    fn test_about() {
        let person: Whoami = Person::new("foobar", vec!["fizz", "buzz"]);
        let about = about(person.name, person.professions());
        assert_eq!(about, "My name is foobar. I am an enthusiastic fizz, buzz.");
    }
}
