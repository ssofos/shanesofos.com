'''Small Class to make Identities'''

class Identity:
  '''Initialize some identities'''
  def __init__(self, given_name, professions):
    self.name = given_name
    self.professions = professions

  def whoami(self):
    '''Print an identity'''
    professions = ', '.join(self.professions)
    print(f'My name is {self.name}. I am an enthusiastic {professions}.')
