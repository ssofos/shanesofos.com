package Static

type Identity struct {
	Name        string
	Professions string
}

var MyIdentity = []Identity{
	{
		Name:        "Shane R. Sofos",
		Professions: "Container Infrastructure Engineer, Application Infrastructure Engineer, Site Reliability Engineer, Systems Engineer, Penguin Wrangler, Pythonist, Rubyist, Rustacean, GNU Herder, Midgardian, Westerosi, Child of Ilúvatar",
	},
}
