Projects
========

Here you will find a collection of active, completed, and upcoming personal projects that I am working on.

All of these projects are one that I have started through my own passions and interests.

Also, the engineering project management of each project is run either by myself or in coordination with any collaborators.


Ongoing
-------

Here are some quick links to my current and ongoing projects:

### Software

:fontawesome-solid-code-branch: [Bouch](./bouch.md){ .md-button }

:fontawesome-solid-code-branch: [Polypass](./polypass.md){ .md-button }

Status
------

Each project will contain a small admonition describing its current status. Here is a guide to the types of project statuses:

!!! done

    This status signifies a project is code complete or done. Enhancements, bug fixes, and major features can still be added to projects that are considered done. I see all projects asephemeral entities, continually being perfected and enhanced as time progresses. Nevertheless it is critical to have finality to certain phases of a project.

!!! todo "Active"

    This status signifies a project that I am actively woking on and that is ongoing.


!!! warning "Stalled"

    This status signifies a project that is currently stalled and I have temporarily stopped active work on.


!!! error "Blocked"

    This status signifies a project that is currently completely blocked from progressing further due to some unknown, unpredictable, or unanticipated event.

!!! bug "Mothballed"

    This status signifies a project that has been completely abandoned or shelved for the foreseeable future, colloquially known as mothballed.

!!! question "Incubated"

    This status signifies a project that is currently incubating and in the process of being fully formalized. It could includes ideas I am excited about, research and developing, or wanted to explore in the future.
