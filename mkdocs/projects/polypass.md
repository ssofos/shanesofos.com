Polypass
=====

!!! done

    This project is code complete.

About
-----

Polypass is [polymorphous](https://en.wiktionary.org/wiki/polymorphous), one that assumes many forms, secure random password generator. It can generate cryptographically secure random passwords like alphanumeric, alphanumeric symbol, and even natural language passwords.

History
-------

Years ago while managing large collections of Linux systems with the principles of Infrastructure as Code, I explored the idea of programmatically generating password hashes for local system accounts like the root superuser. My main goal was to utilize a [cryptographically secure pseudo-random number generator (CSPRNG)](https://en.wikipedia.org/wiki/Cryptographically_secure_pseudorandom_number_generator) to generate [crypt](https://ruby-doc.org/core-2.5.1/String.html#method-i-crypt) compatible password hashes.

My fascination with the concepts of secure randomness and the guarantee of perfect secrecy collided with an affinity for the Ruby Programming Language. This lead me down a path of writing a simple 80 line Ruby Class with some basic helper methods using a modified version of the [random_password_generator RubyGem](https://rubygems.org/gems/random_password_generator) and fusing it with [Ruby's SecureRandom module](https://ruby-doc.org/stdlib-2.5.1/libdoc/securerandom/rdoc/SecureRandom.html). Ultimately, I achieved my original goal and it worked great for what it was used for: secure, random, and ephemeral generation of root user password hashes.

Fast forward years later, add an interest in Natural Language Processing, a much deeper understanding of the Ruby Programming Language, the same passion for secure randomness, a general desire to create and share free software, and you end up with a RubyGem called **Polypass**.

When writing Polypass I really wanted to incorporate a fun facet of [Natural Language Processing](https://en.wikipedia.org/wiki/Natural_language_processing) into the generation of secure and random passwords. Fortunately, there are no lack of resources regarding NLP and I came across a very elegant and lightweight RubyGem called [literate_randomizer](https://rubygems.org/gems/literate_randomizer).

Literate_randomizer combines the probabilistic model of [Markov Chains](https://en.wikipedia.org/wiki/Markov_chain) with the natural language of free literature from [Project Gutenberg](https://www.gutenberg.org) to generate near-English prose random sentences.

Polypass leverages this RubyGem with a cryptographically secure pseudo-random number generator, and some Project Gutenberg supplied literary classics like [Dracula](https://www.gutenberg.org/ebooks/345), [Frankenstein](https://www.gutenberg.org/ebooks/84), [The Iliad](https://www.gutenberg.org/ebooks/6130), which allows it to make some pretty amusing, spontaneous, and quite secure passwords.

Name
----

Polypass is the [Portmanteau](https://en.wikipedia.org/wiki/Portmanteau) of the words **poly** and **password**. The word [Poly's](https://en.wiktionary.org/wiki/poly-#English) etymological roots come from the Ancient Greek word *polús*, meaning many, much. Polypass literally means *many passwords*.

Natural Language Processing
---------------------------

* [Ruby Natural Language Processing Resources](https://github.com/diasks2/ruby-nlp)

Password Security
-----------------

* [How Secure Is My Password?](https://howsecureismypassword.net/)
* [Password Strength XKCD](https://xkcd.com/936/)
* [Password Cracking Wikipedia Page](https://en.wikipedia.org/wiki/Password_cracking)
* [Password Wikipedia Page](https://en.wikipedia.org/wiki/Password)

RubyGem
-------

* [Polypass RubyGem](https://rubygems.org/gems/polypass)

Source Code
-----------

* [Polypass GitLab Repository](https://gitlab.com/ssofos/polypass)
