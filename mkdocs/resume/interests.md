Interests
=========

* [Linux Operating Systems](https://en.wikipedia.org/wiki/Linux_kernel).
* Linguistics, etymology, mythology, history, penmanship, and [autodidacticism](https://en.wikipedia.org/wiki/Autodidacticism).
* [The J.R.R. Tolkien Legendarium](https://en.wikipedia.org/wiki/Tolkien%27s_legendarium).
* [A Song of Ice and Fire](https://en.wikipedia.org/wiki/A_Song_of_Ice_and_Fire).
* [The Sprawl](https://en.wikipedia.org/wiki/Sprawl_trilogy).
* [Snow Crash](https://en.wikipedia.org/wiki/Snow_Crash).
