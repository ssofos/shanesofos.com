Skill Set
=========

* 2.5+ years of experience in building, deploying, maintaining, and leveraging the Prometheus Telemetry Platform (Prometheus, Alertmanager, Exporters, and Grafana) for real-time application metrics analysis
* 2.5+ years of experience in building, automating, deploying containerized full-stack polyglot applications on-top of Kubernetes infrastructure
* 3+ years of experience in developing, testing, and deploying Python Flask micro-services for Kubernetes environments
* 3+ years of experience in developing, testing, and deploying Ruby Sinatra Rack web applications and micro-services for Kubernetes environments
* 3+ years of experience in developing and deploying Heroku style applications
* 4+ years of experience in using data analytics logging platforms and tools like Splunk and Fluent Bit
* 4+ years of experience in developing, building, testing, and publishing Docker based application container images automatically with Continuous Integration & Continuous Delivery (CI/CD)
* 4+ years of experience in developing, building, testing, packaging, and publishing RubyGems and PyPi software with CI/CD
* 4+ years of experience and expertise in systems and application deployment automation with the Chef Infrastructure as Code (IaC) framework
* 5+ years of experience and expertise in systems and application deployment automation with the Ansible IaC framework
* 5+ years of experience in developing and deploying Nginx powered web applications
* 9+ years of experience with the Ruby Programming Language
* 10+ years in highly demanding and fast development/operations production level environments
* 10+ years of experience in building, developing, and operating large scale global multi-regional active-active infrastructure
* 10+ years of experience and intimate familiarity in code review processes, principals and workflows
* 10+ years of experience and expertise with distributed revision control systems like git
* 12+ years of BASH scripting in production environments
* 15+ years of GNU/Linux Systems Administration
* 16+ years GNU/Linux OS Usage
* 17+ years Computer Systems Administration
* Expertise in embedding with deeply technical software engineering teams and engineering application infrastructure for those teams
* Expertise in engineering and operating in multi-operating-system, multi-architecture, polyglot software engineering environments
* Expertise with practicing, testing and implementing the principals Infrastructure as Code
* Expertise in container based application development, testing, and deployment in environments like Docker and Kubernetes
* Expertise in designing, testing, implementing, and leveraging automation frameworks like Chef and Ansible primarily (Puppet and Salt secondarily) across global multi-thousand node environments
* Expertise in systems package management, artifact generation, and artifact distribution
* Expertise in GNU/Linux Operating System fundamentals
* Expertise in writing detailed, useful technical documentation
* Expertise in usage and deployment of static site generation software for automated and codified technical documentation
* Expertise in writing and testing Object Oriented programming languages such as Ruby and Python
* Expertise in maintaining, distributing, and releasing software written in compiled programming languages
* Expertise in using Behavior-driven Test-driven Development processes and techniques with testing frameworks like  RSpec and Pytest
* Expertise in engineering driven incident/crisis response management, root cause analysis, and post incident review creation
* Acute analytical approach to solving and debugging technical problems
* Data driven philosophy when problem solving complex systems and applications incident scenarios
* Advanced and excellent inter-personal, communication, written, and language skills
* Advanced and excellent organizational and engineering project management skills
