Objective
=========

To effectively contribute to the ever evolving world of computer information systems and software engineering while practicing my personal work philosophy of **FOSPAAR**:

* **F**ocus
* **O**rganization
* **S**incerity
* **P**atience
* **A**daptiveness
* **A**wareness
* **R**espect

All while continually learning, growing, evolving, applying, and sharing my knowledge and skill set to build and ship amazingly creative, high quality software on a dynamic professional teams.
