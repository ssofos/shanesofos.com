#!/usr/bin/env python
'''Print out my identity'''

from identity import Identity

person = Identity(
    'Shane R. Sofos',
    ['Container Infrastructure Engineer',
     'Site Reliability Engineer',
     'Systems Engineer',
     'Penguin Wrangler',
     'Pythonist',
     'Rubyist',
     'Rustacean',
     'GNU Herder',
     'Midgardian',
     'Westerosi',
     'and Child of Ilúvatar']
)
person.whoami()
