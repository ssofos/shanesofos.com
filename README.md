[![pipeline status](https://gitlab.com/ssofos/shanesofos.com/badges/master/pipeline.svg)](https://gitlab.com/ssofos/shanesofos.com/commits/master) [![coverage report](https://gitlab.com/ssofos/shanesofos.com/badges/master/coverage.svg)](https://gitlab.com/ssofos/shanesofos.com/commits/master)

About
=====

This is the codified personal portfolio website for Shane R. Sofos.

The source code includes auto-generated static web content, static site generator configurations, infrastructure automation configurations, examples, and tools to create [shanesofos.com](https://shanesofos.com)

Components
==========

* Web Hosting Platform: [Cloudflare Workers Sites](https://developers.cloudflare.com/workers/platform/sites)
* Resume Generator:
    * Custom Ruby Resume Markdown Generator Library
    * [Erubis Template Engine](http://www.kuwata-lab.com/erubis/)
    * [Gimli](https://github.com/walle/gimli)
    * Custom Ruby Wrapper Script
* Resume Format: [CommonMark Markdown](http://commonmark.org/)
* Static Site Generator: [MkDocs](http://www.mkdocs.org/)
* Site Theme: [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/)
* Testing Framework: [RSpec](http://rspec.info/) & [Pytest](https://docs.pytest.org/)
* Other Tools:
    * [Python Programming Language](https://www.python.org/)
    * [Ruby Programming Language](https://www.ruby-lang.org/)
    * [Rust Programming Language](https://www.rust-lang.org/)
    * [Go Programming Language](https://go.dev/)
    * [Docker](https://docs.docker.com/)
    * [Bundler](https://bundler.io/)
    * [Rake](https://ruby.github.io/rake/)
    * [RuboCop](http://batsov.com/rubocop/)
    * [Wrangler](https://github.com/cloudflare/wrangler)
    * [typos-cli](https://crates.io/crates/typos-cli)

Domains
=======

* [shanesofos.com](https://www.shanesofos.com/)
* [shanesofos.net](https://www.shanesofos.net/)
* [shanesofos.info](https://www.shanesofos.info/)

Contact
=======

If you have any problems, questions, ideas, suggestions, please contact me via email at **ssofos[at]gmail[dot]com**

License
=======

Unless otherwise displayed, noted or commented, All rights are reserved by Shane R. Sofos.
